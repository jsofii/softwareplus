import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";

import "assets/scss/material-kit-react.scss?v=1.9.0";

// pages for this product
import Components from "views/Components/Components.js";
import LandingPage from "views/LandingPage/LandingPage.js";
import ProfilePage from "views/ProfilePage/ProfilePage.js";
import ProfilePageKelvin from "views/ProfilePage/ProfilePageKelvin.js";
import ProfilePageSofi from "views/ProfilePage/ProfilePageSofi.js";
import ProfilePageMarco from "views/ProfilePage/ProfilePageMarco.js";
import ProfilePageCris from "views/ProfilePage/ProfilePageCris.js";
import ProfilePageCesar from "views/ProfilePage/ProfilePageCesar.js";
import ProfilePageDiego from "views/ProfilePage/ProfilePageDiego.js";
import ProfilePageClaudio from "views/ProfilePage/ProfilePageClaudio.js";
import LoginPage from "views/LoginPage/LoginPage.js";

var hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/landing-page" component={LandingPage} />
      <Route path="/profile-page" component={ProfilePage} />
      <Route path="/profile-page-kelvin" component={ProfilePageKelvin} />
      <Route path="/profile-page-sofi" component={ProfilePageSofi} />
      <Route path="/profile-page-marco" component={ProfilePageMarco} />
      <Route path="/profile-page-cristian" component={ProfilePageCris} />
      <Route path="/profile-page-cesar" component={ProfilePageCesar} />
      <Route path="/profile-page-diego" component={ProfilePageDiego} />
      <Route path="/profile-page-claudio" component={ProfilePageClaudio} />
      <Route path="/login-page" component={LoginPage} />
      <Route path="/" component={Components} />
    </Switch>
  </Router>,
  document.getElementById("root")
);
